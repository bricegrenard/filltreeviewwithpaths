using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace FillTreeviewConsole
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Dictionary<string, string> csvDictionary = new Dictionary<string, string>();
            using (StreamReader reader = new StreamReader("C:\\Albums.csv"))
            {
                while (!reader.EndOfStream)
                {
                    string[] values = reader.ReadLine()?.Split(';');
                    csvDictionary.Add(values[0], values[1]);
                }
            }
            TreeviewStructure treeviewStructure = FindRootNode(csvDictionary);
            if (treeviewStructure != null)
            {
                foreach (KeyValuePair<string, string> item in csvDictionary)
                {
                    string value = item.Value;
                    if (value.Contains("/"))
                    {
                        string[] splittedValues = value.Split("/", StringSplitOptions.RemoveEmptyEntries);
                        if (splittedValues.Length != 0)
                        {
                            FillTree(ref treeviewStructure, splittedValues.Where((string x) => x != treeviewStructure.Header).ToList());
                        }
                    }
                }
                Print(treeviewStructure, "  ", last: false);
            }
        }

        public static void Print(TreeviewStructure node, string indent, bool last)
        {
            Console.Out.WriteLine(indent);
            if (last)
            {
                Console.Out.Write("\\-");
                indent += "  ";
            }
            else
            {
                Console.Out.Write("|-");
                indent += "| ";
            }
            Console.Out.Write(node.Header + Environment.NewLine);
            if (node.Children != null)
            {
                List<TreeviewStructure> Children = node.Children;
                for (int i = 0; i < Children.Count; i++)
                {
                    Print(Children[i], indent, i == Children.Count - 1);
                }
            }
        }

        private static void FillTree(ref TreeviewStructure root, List<string> values)
        {
            while (values.Count != 0)
            {
                if (root != null && root.Children != null && root.Children.Any((TreeviewStructure x) => x.Header == values[0]))
                {
                    TreeviewStructure existingchild = root.Children.FirstOrDefault((TreeviewStructure x) => x.Header == values[0]);
                    if (existingchild != null)
                    {
                        values.RemoveAt(0);
                        FillTree(ref existingchild, values);
                    }
                    continue;
                }
                TreeviewStructure child = new TreeviewStructure
                {
                    Header = values[0]
                };
                if (root.Children == null)
                {
                    root.Children = new List<TreeviewStructure>();
                }
                root.Children.Add(child);
                values.RemoveAt(0);
                FillTree(ref child, values);
            }
        }

        private static TreeviewStructure FindRootNode(Dictionary<string, string> csvDictionary)
        {
            List<string> rootValues = new List<string>();
            foreach (string value in csvDictionary.Values)
            {
                if (value.Contains("/"))
                {
                    string[] splittedValues = value.Split("/", StringSplitOptions.RemoveEmptyEntries);
                    if (splittedValues.Length != 0)
                    {
                        rootValues.Add(splittedValues[0]);
                    }
                }
            }
            if (rootValues.Distinct().Count() == 1)
            {
                return new TreeviewStructure
                {
                    Header = rootValues[0],
                    Children = new List<TreeviewStructure>()
                };
            }
            return null;
        }
    }
}

using System.Collections.Generic;

namespace FillTreeviewConsole
{
    public class TreeviewStructure
    {
        public string Header
        {
            get;
            set;
        }

        public List<TreeviewStructure> Children
        {
            get;
            set;
        }
    }
}
